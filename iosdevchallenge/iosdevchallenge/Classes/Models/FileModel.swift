//
//  FileModel.swift
//  iosdevchallenge
//
//  Created by Станислав Редреев on 4/18/16.
//  Copyright © 2016 Stanislav Redreiev / Станислав Редреев. All rights reserved.
//

import UIKit

class FileModel {

    var filename: String?
    var isFolder: Bool?
    var modDate: NSDate?
    var fileType: FileType?
    var isOrange: Bool
    var isBlue: Bool?
    
    init(fileName: String, isFolder: Bool, modDate: NSDate, fileType: FileType, isOrange: Bool, isBlue: Bool) {
        self.filename = fileName
        self.isFolder = isFolder
        self.modDate = modDate
        self.fileType = fileType
        self.isOrange = isOrange
        self.isBlue = isBlue
    }
    
}
