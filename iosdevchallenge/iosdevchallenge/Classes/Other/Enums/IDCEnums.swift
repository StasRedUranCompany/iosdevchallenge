//
//  IDCEnums.swift
//  iosdevchallenge
//
//  Created by Станислав Редреев on 4/18/16.
//  Copyright © 2016 Stanislav Redreiev / Станислав Редреев. All rights reserved.
//

import Foundation

enum FileType {
    case Image
    case Pdf
    case Movie
    case Music
    case Unknown
}
