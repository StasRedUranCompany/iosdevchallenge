//
//  IDCMainManager.swift
//  iosdevchallenge
//
//  Created by Станислав Редреев on 4/18/16.
//  Copyright © 2016 Stanislav Redreiev / Станислав Редреев. All rights reserved.
//

import UIKit

class IDCMainManager {

    // MARK: - Singleton
    
    class var defaultManager: IDCMainManager {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: IDCMainManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = IDCMainManager()
        }
        return Static.instance!
    }
    
    init() {
        self.mainStoryboardInstance = UIStoryboard(name: "Main", bundle: nil)
        self.localDataManagerInstance = IDCFileManager()
    }
    
    // MARK: - Private Properties
    
    private var localDataManagerInstance: IDCFileManagerProtocol
    private var mainStoryboardInstance: UIStoryboard
    
    // MARK: - Interface
    
    class var localDataManager: IDCFileManagerProtocol {
        return IDCMainManager.defaultManager.localDataManagerInstance
    }
    
    // MARK: - Storyboards
    
    struct Storyboards {
        static var mainStoryboard: UIStoryboard {
            return IDCMainManager.defaultManager.mainStoryboardInstance
        }
    }

}
