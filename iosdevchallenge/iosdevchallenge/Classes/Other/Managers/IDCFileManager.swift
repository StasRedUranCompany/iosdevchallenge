//
//  IDCFileManager.swift
//  iosdevchallenge
//
//  Created by Станислав Редреев on 4/18/16.
//  Copyright © 2016 Stanislav Redreiev / Станислав Редреев. All rights reserved.
//

import UIKit

class IDCFileManager: IDCFileManagerProtocol {
    
    func getRootContent() -> [FileModel] {
        return IDCTestDataManager.getTestRootData()
    }
    
    func getContentForPath(path: String) -> [FileModel] {
        return []
    }
    
    func getContentForParent(parent: String) -> [FileModel] {
        return []
    }

}
