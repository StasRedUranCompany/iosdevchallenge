//
//  UIViewController+ID.swift
//  iosdevchallenge
//
//  Created by Станислав Редреев on 4/18/16.
//  Copyright © 2016 Stanislav Redreiev / Станислав Редреев. All rights reserved.
//

import UIKit

extension UIViewController {
    class func identifier() -> String {
        return String(self)
    }
}