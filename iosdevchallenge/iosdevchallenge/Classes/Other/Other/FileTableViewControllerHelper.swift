//
//  FileTableViewControllerHelper.swift
//  iosdevchallenge
//
//  Created by Станислав Редреев on 4/18/16.
//  Copyright © 2016 Stanislav Redreiev / Станислав Редреев. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class FileTableViewControllerHelper: NSObject {

    class func rightSubmenuButtons() -> [MGSwipeButton] {
        return [
            MGSwipeButton(title: "", icon: UIImage(named:"star.png"), backgroundColor: UIColor.whiteColor()),
            MGSwipeButton(title: "", icon: UIImage(named:"infinity.png"), backgroundColor: UIColor.whiteColor()),
            MGSwipeButton(title: "", icon: UIImage(named:"trash.png"), backgroundColor: UIColor.whiteColor())
        ]
    }
}
