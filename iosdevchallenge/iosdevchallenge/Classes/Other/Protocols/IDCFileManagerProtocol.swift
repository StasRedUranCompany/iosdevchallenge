//
//  IDCFileManagerProtocol.swift
//  iosdevchallenge
//
//  Created by Станислав Редреев on 4/18/16.
//  Copyright © 2016 Stanislav Redreiev / Станислав Редреев. All rights reserved.
//

import Foundation

protocol IDCFileManagerProtocol {
    
    func getRootContent() -> [FileModel]
    func getContentForPath(path: String) -> [FileModel]
    func getContentForParent(parent: String) -> [FileModel]
    
}
