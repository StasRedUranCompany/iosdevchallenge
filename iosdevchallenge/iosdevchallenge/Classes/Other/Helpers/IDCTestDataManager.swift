//
//  IDCTestDataManager.swift
//  iosdevchallenge
//
//  Created by Станислав Редреев on 4/18/16.
//  Copyright © 2016 Stanislav Redreiev / Станислав Редреев. All rights reserved.
//

import UIKit

class IDCTestDataManager: NSObject {
    
    class func getTestRootData() -> [FileModel] {
        return [
            FileModel(fileName: "File1", isFolder: false, modDate: NSDate(), fileType: FileType.Image, isOrange: true, isBlue: false),
            FileModel(fileName: "File2", isFolder: false, modDate: NSDate(), fileType: FileType.Music, isOrange: true, isBlue: false),
            FileModel(fileName: "File3", isFolder: false, modDate: NSDate(), fileType: FileType.Image, isOrange: true, isBlue: true),
            FileModel(fileName: "File4", isFolder: false, modDate: NSDate(), fileType: FileType.Image, isOrange: false, isBlue: true),
            FileModel(fileName: "File5", isFolder: false, modDate: NSDate(), fileType: FileType.Image, isOrange: true, isBlue: true)
        ]
    }

}
