//
//  IDCFileCell.swift
//  iosdevchallenge
//
//  Created by Станислав Редреев on 4/18/16.
//  Copyright © 2016 Stanislav Redreiev / Станислав Редреев. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class IDCFileCell: MGSwipeTableCell {
    
    @IBOutlet var icon: UIImageView!
    @IBOutlet var filenameLabel: UILabel!
    @IBOutlet var modifierDateLabel: UILabel!
    @IBOutlet var orangeIndicator: UIView!
    @IBOutlet var blueIndicator: UIView!
    
    func fillCell(fileModel fileModel: FileModel) {
        self.icon.image = IDCMainHelper.getIconForFileType(fileModel.fileType ?? FileType.Unknown)
        self.filenameLabel.text = fileModel.filename
    }
    
    private func setupIndicators(fileModel: FileModel) {
        
    }

}
