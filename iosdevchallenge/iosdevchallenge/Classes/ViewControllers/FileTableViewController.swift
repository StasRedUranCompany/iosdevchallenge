//
//  FileTableViewController.swift
//  iosdevchallenge
//
//  Created by Станислав Редреев on 4/18/16.
//  Copyright © 2016 Stanislav Redreiev / Станислав Редреев. All rights reserved.
//

import UIKit

import MGSwipeTableCell

class FileTableViewController: UITableViewController {
    
    var documentsPath: String?
    var content: [FileModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getFiles()
    }
    
    // Private
    
    private func getFiles() {
        if let path = self.documentsPath {
            self.content = IDCMainManager.localDataManager.getContentForPath(path)
        } else {
            self.content = IDCMainManager.localDataManager.getRootContent()
        }
        
        self.tableView.reloadData()
    }
}

extension FileTableViewController {
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCellWithIdentifier("IDCFileCell", forIndexPath: indexPath) as? IDCFileCell {
            
            let fileModel = self.content[indexPath.row]
            cell.fillCell(fileModel:fileModel)
            cell.delegate = self
            
            self.addAndSetupSubmenuButtonsToCell(cell)
            return cell
        }
        
        return UITableViewCell()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    private func addAndSetupSubmenuButtonsToCell(cell: IDCFileCell) {
        cell.rightButtons = FileTableViewControllerHelper.rightSubmenuButtons()
        cell.rightSwipeSettings.transition = MGSwipeTransition.Drag
    }
}

extension FileTableViewController: MGSwipeTableCellDelegate {
    func swipeTableCell(cell: MGSwipeTableCell!, tappedButtonAtIndex index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {
        debugPrint("Button tapped")
        return true
    }
}
